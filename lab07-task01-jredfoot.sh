#!/usr/bin/bash

read -p "Are you ready to play craps? (y or n): " answer
input=$answer
while [ "$input" = "y" ]; do

	number1=$((RANDOM % 6+1))
	number2=$((RANDOM % 6+1))
	sum=$(( $number1 + $number2 ))
	echo "The dice rolled a $sum."
	read -p "Do you want to continue? (y or n): " answer
	input=$answer
	if [ $sum = 2 ] || [ $sum = 3 ] || [ $sum = 12 ]; then
	echo "Because the sum was $sum, you loose!"
	read -p "Want to roll again? (y or n): " answer
	input=$answer
	fi
	if [ $sum = 7 ] || [ $sum = 11 ]; then
	echo "Because the sum was $sum, you are a Winner, Winner, Chicken Dinner!!!!"
	read -p "Want to roll again? (y or n): " answer
	input=$answer
	fi
	if [ "$answer" = "n" ] || [ "$answer" = "" ];  then
	echo "Come back when you're ready to play!"
	fi
done
